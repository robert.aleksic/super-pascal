{$mode objfpc}
program compile (input, output);
uses
  sysutils, //for exceptions
  common,
  scan, parse, assembly;

begin
  try
    if paramcount<1 then error ('syntax: compile fname [t]');
    fname := paramstr (1);
    debug := paramcount>1;
    if debug then rewrite (logf, fname+'.log');

    scanpass;
    nextpass;

    parsepass;
    nextpass;

    assemblypass;
    nextpass;

    except on e:exception do halt (1)
  end
end.