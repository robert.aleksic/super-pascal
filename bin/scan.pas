{$mode objfpc}
unit scan;

interface

  procedure scanpass;

implementation
uses
  sysutils, common;

  procedure scanpass;

  const
    hashmax = 631;

  const
    eol = #10; eot = #3; //lf=ctrl-n, eot=ctrl-c

    separators  = [' ', eol, '{'];
    endline     = [eol, eot];
    endcomment  = ['}', eot];

    digits         = ['0'..'9'];
    smallletters   = ['a'..'z'];
    capitalletters = ['A'..'Z'];

    letters      = capitalletters + smallletters;
    alphanumeric = letters + digits;

    invisible = [null..' ',del] - [' ', eol, eot];

  type
    strtable = array [1..maxchar] of char;

    wordref = ^ wordrec;
    wordrec = record
                id   : integer;
                rsvd : boolean;

                firstch : integer;
                length  : integer;

                next : wordref
              end;

    hashtable = array [1..hashmax] of wordref;

  var
    sourcef : text;

    strt    : strtable;
    strtlen : integer;

    hasht  : hashtable;
    currid : integer;

    afterperiod : boolean;
    newline     : boolean;

    ch : char;



    procedure getch (var ch: char);
    begin
      if eof (sourcef)
      then ch := eot
      else begin
             if newline
             then begin
                    newline := false;

                    currl  := currl + 1;
                    currch := 0;

                    if debug then write (logf, currl:6, ' ')
                  end;

             if not eoln (sourcef)
             then begin
                    read (sourcef, ch);
                    currch := currch+1;
                    if debug then write (logf, ch)
                  end
             else begin
                    readln (sourcef);
                    ch := eol;

                    newline := true;
                    if debug then writeln (logf)
                  end
             end
    end;

    procedure nextch;
    var skip : boolean;
    begin
      repeat
        getch (ch);
        if (ch < null) or (ch > del)
        then skip := true
        else skip := ch in invisible
      until not skip
    end;



    procedure emit1 (rsv : integer);
    begin put (rsv) end;

    procedure emit2 (rsv, arg : integer);
    begin
      put (rsv); put (arg)
    end;

    procedure emitreal (val : real);
    begin
      put (realconst1); putreal (val)
    end;

    procedure emitstring (val: string);
    begin
      put (stringconst1); putstring (val)
    end;



    function hash (s:string) : integer;
    const modbase = 32768-127;
    var sum, l, i: integer;
    begin
      l := length (s);
      sum := 0;
      for i := 1 to l do
        sum := ( sum + ord (s[i]) ) mod modbase;
      hash := sum mod hashmax + 1
    end;

    procedure appendid (s:string; idt:integer; rsv:boolean);
    var wr : wordref; pos, l, i, h: integer;
    begin
      h := hash (s);
      l := length(s);

      pos     := strtlen+1;
      strtlen := strtlen + l;
      if strtlen > maxchar then error (maxchar5);

      for i := 1 to l do
        strt [pos+i-1] := s [i];

      new (wr);
      with wr^ do
      begin
        id   := idt;
        rsvd := rsv;

        firstch := pos;
        length  := l;

        next := hasht [h]
      end;
      hasht [h] := wr
    end;

    procedure addrsv (s:string; id:integer);
    begin
      appendid (s, id,true)
    end;

    procedure addid (s:string; id:integer);
    begin
      appendid (s, id,false)
    end;

    procedure search (s:string; var id:integer; var rsv:boolean);
    var wr : wordref; h : integer; done : boolean;

      function equal (wr:wordref; s:string) : boolean;
      var pos, l,len : integer;
      begin
        len := length (s);
        if wr^.length <> len
        then equal := false
        else begin
               equal := true;

               pos := wr^.firstch; l := 0;
               while equal and (l<len) do
               begin
                 l := l+1;
                 equal := s[l] = strt[pos];
                 pos := pos+1
               end
             end
      end;

    begin
      h  := hash (s);
      wr := hasht [h];

      done := false;
      while not done do
        if wr <> nil
        then if not equal (wr, s)
             then wr := wr^.next
             else begin
                    id  := wr^.id;
                    rsv := wr^.rsvd;

                    done := true
                  end
        else begin
               currid := currid + 1;

               id  := currid;
               rsv := false;
               addid (s, id);

               done := true
             end
    end;

    { Wordrsvbol = "and" | "array" | "assume" | "begin" | "case" | "const" |
        "div" | "do" | "downto" | "else" | "end" | "for" | "forall" | "function" | "if" |
        "mod" | "not" | "of" | "or" | "parallel" | "procedure" | "program" | "record" |
        "repeat" | "sic" | "then" | "to" | "type" | "until" | "var" | "while"
      UnusedWord = "file" | "goto" | "in" | "label" | "nil" | "packed" | "set" | "with" }

    procedure init;
    var i: integer;
    begin
      for i := 1 to hashmax do hasht [i] := nil;
      strtlen := 0;

      // reserved words
      addrsv ('and',       and1);     addrsv ('array',    array1);    addrsv ('assume',    assume1);
      addrsv ('begin',     begin1);   addrsv ('case',     case1);     addrsv ('const',     const1);
      addrsv ('div',       div1);     addrsv ('do',       do1);       addrsv ('downto',    downto1);
      addrsv ('else',      else1);    addrsv ('end',      end1);      addrsv ('file',      unknown1);
      addrsv ('for',       for1);     addrsv ('forall',   forall1);   addrsv ('function',  function1);
      addrsv ('goto',      unknown1); addrsv ('if',       if1);       addrsv ('in',        unknown1);
      addrsv ('label',     unknown1); addrsv ('mod',      mod1);      addrsv ('nil',       unknown1);
      addrsv ('not',       not1);     addrsv ('of',       of1);       addrsv ('or',        or1);
      addrsv ('packed',    unknown1); addrsv ('parallel', parallel1); addrsv ('procedure', procedure1);
      addrsv ('program',   program1); addrsv ('record',   record1);   addrsv ('repeat',    repeat1);
      addrsv ('set',       unknown1); addrsv ('sic',      sic1);      addrsv ('then',      then1);
      addrsv ('to',        to1);      addrsv ('type',     type1);     addrsv ('until',     until1);
      addrsv ('var',       var1);     addrsv ('while',    while1);    addrsv ('with',      unknown1);

      // standard identifiers
      addid ('abs',     abs0);    addid ('arctan',    arctan0);    addid ('boolean', boolean0);
      addid ('char',    char0);   addid ('chr',       chr0);       addid ('cos',     cos0);
      addid ('eof',     eof0);    addid ('eoln',      eoln0);      addid ('exp',     exp0);
      addid ('false',   false0);  addid ('integer',   integer0);   addid ('ln',      ln0);
      addid ('maxint',  maxint0); addid ('maxstring', maxstring0); addid ('null',    null0);
      addid ('odd',     odd0);    addid ('open',      open0);      addid ('ord',     ord0);
      addid ('pred',    pred0);   addid ('read',      read0);      addid ('readln',  readln0);
      addid ('real',    real0);   addid ('receive',   receive0);   addid ('round',   round0);
      addid ('send',    send0);   addid ('sin',       sin0);       addid ('sqr',     sqr0);
      addid ('sqrt',    sqrt0);   addid ('string',    string0);    addid ('succ',    succ0);
      addid ('true',    true0);   addid ('trunc',     trunc0);     addid ('write',   write0);
      addid ('writeln', writeln0);

      currid := maxstandard;

      reset (sourcef, fname + sourceext);
      outbuf.len := 0;

      afterperiod := false;
      currl := 0; currch := 0;
      newline := true;
      nextch
    end;

    procedure done;
    var i : integer; p,q : wordref;
    begin
      close (sourcef);

      for i := 1 to hashmax do
      begin
        p := hasht [i];
        while p<>nil do
        begin
          q := p^.next;
          dispose (p);
          p := q
        end
      end
    end;

    // LEXICAL ANALYSIS

    { Comment = LeftBrace [ CommentElement ]* RightBrace
      CommentElement = GraphicCharacter | NewLine | Comment }
    procedure comment;
    begin // ch = {
      nextch;

      while not (ch in endcomment) do
        if ch = '{' // nexted comment
        then comment
        else if ch <> eol
             then nextch
             else begin nextch; emit2 (newline1, currl) end;

      if ch <> '}'
      then error (comment3);

      nextch
    end;

    { Word = Wordrsvbol | Identifier
      Identifier = Letter [ Letter | Digit ]* }
    procedure word;
    var
      s : string;
      l : integer;

      id  : integer;
      rsv : boolean;

    begin // ch in letters
      s := ''; l := 0;
      while ch in alphanumeric do
      begin
        if ch in capitalletters
        then ch := chr (ord(ch) - ord('A') + ord ('a'));

        if l = maxstring then error (maxstring5);
        l := l+1; s := s+ch;

        nextch
      end;

      search (s, id, rsv);

      if rsv
      then emit1 (id)
      else emit2 (identifier1, id)
    end;

    { DigitSequence = Digit [ Digit ]* }
    procedure digitsequence (var r:real; var n:integer);
    var d: real;
    begin // ch in digits
      r := 0.0; n := 0;
      if not (ch in digits)
      then error (number3);

      while ch in digits do
      begin
        d := ord(ch) - ord('0');
        r := 10.0 * r + d;
        n := n + 1;
        nextch
      end
    end;

    { UnsignedNumber  = UnsignedReal | UnsignedInteger
      UnsignedInteger = DigitSequence
      UnsignedReal    = IntegerPart RealOption
      IntegerPart     = DigitSequence
      RealOption      = "." FractionalPart [ ScalingPart ] | ScalingPart
      FractionalPart  = DigitSequence

      ScalingPart     = Radix ScaleFactor
      Radix           = "e" | "E"

      ScaleFactor         = [ Sign ] UnsignedScaleFactor
      UnsignedScaleFactor =  DigitSequence
      Sign                =  "+" | "-" }
    procedure unsignednumber;
    var
      i, r, f : real;
      s, n    : integer;

      procedure unsignedinteger (r:real);
      var i: integer;
      begin
        if r > maxint
        then error (number3);
        i := trunc(r);
        emit2 (intconst1, i)
      end;

      procedure scalefactor (var s:integer);

        procedure unsignedscalefactor (var s: integer);
        var r: real; n: integer;
        begin
          digitsequence (r, n);
          if r > maxint then
            begin
              error (number3);
              s := 0
            end
          else s := trunc(r)
        end;

      begin
        case ch of
          '+' : begin
                  nextch;
                  unsignedscalefactor (s)
                end;
          '-' : begin
                  nextch;
                  unsignedscalefactor(s);
                  s := - s
                end
          else unsignedscalefactor(s)
        end
      end;

      function scaled (r:real; s:integer) : real;
      var max, min: real;
      begin
        max := maxreal / 10.0;
        while s > 0 do
        begin
          if r > max then error (number3);
          r := r * 10.0;
          s := s - 1
        end;
        min := 10.0 * minreal;
        while s < 0 do
        begin
          if r >= min
          then r := r / 10.0
          else r := 0.0;
          s := s + 1
        end;
        scaled := r
      end;

    begin
      digitsequence (i, n);

      if ch <> '.'
      then if not (ch in ['e', 'E'])
           then unsignedinteger (i)
           else begin // i * 10**s
                  nextch;
                  scalefactor (s);
                  r := scaled (i, s);
                  emitreal (r)
                end
      else begin
             nextch;
             if ch = '.'
             then begin // i..
                    unsignedinteger (i);
                    afterperiod := true
                  end
             else begin // r = i.f
                    digitsequence (f, n);
                    r := i + scaled (f, -n);

                    if ch in ['e', 'E'] // i.f * 10**s
                    then begin
                           nextch;
                           scalefactor (s);
                           r := scaled (r, s)
                        end;
                    emitreal(r)
                  end
           end
    end;


    { CharacterString = "'" StringElements "'"
      StringElements  = StringElement [StringElement]*
      StringElement = StringCharacter | ApostropheImage
      ApostropheImage = "''" }
    procedure characterstring;
    type
      state = (extend, accept, reject);
    var
      st : state; s : string;

      procedure addch;
      begin
        if length(s) = maxstring
        then error (maxstring5);

        s := s+ch;
        nextch
      end;

    begin
      nextch;

      s  := '';
      st := extend;

      while st = extend do
        if ch in endline
        then st := reject
        else if ch <> ''''
             then addch
             else begin
                    nextch;
                    if ch = ''''
                    then addch
                    else st := accept
                  end;

      if (st = reject) or (s = '')
      then emit1 (unknown1)
      else if length(s) = 1
           then emit2 (charconst1, ord (s[1]))
           else emitstring (s)
    end;

    { TokenField = [ Separator ]* Token .
      Token = Literal | Identifier | Specialrsvbol | UnknownToken | EndText
      Literal = UnsignedNumber | CharacterString
      Specialrsvbol =  "(" | ")" | "*" | "+" | "," | "-" | "." | "/" | ":" | ";" |
        "<" | "=" | ">" | "[" | "]" | ".." | ":=" | "<=" | "<>" | ">=" | "|" | Wordrsvbol
      UnknownToken = UnusedWord | UnusedCharacter
      UnusedCharacter = "!" | """ | "#" | "$" | "%" | "&" | "?" | "@" | "\" | "^" | "_" | "`" | "~" }
    procedure nexttoken;
    begin
      while ch in separators do
        case ch of
          ' ' : nextch;
          '{' : comment;
          eol : begin
                  nextch;
                  emit2 (newline1, currl)
                end
        end;

      if ch in letters
      then word
      else if ch in digits
           then unsignednumber
           else case ch of
                  '''' : characterstring;
                  '+'  : begin emit1 (plus1); nextch end;
                  '-'  : begin emit1 (minus1); nextch end;
                  '*'  : begin emit1 (asterisk1); nextch end;
                  '/'  : begin emit1 (slash1); nextch end;
                  '('  : begin emit1 (leftparenthesis1); nextch end;
                  ')'  : begin emit1 (rightparenthesis1); nextch end;
                  '['  : begin emit1 (leftbracket1); nextch end;
                  ']'  : begin emit1 (rightbracket1); nextch end;
                  ','  : begin emit1 (comma1); nextch end;
                  '='  : begin emit1 (equal1); nextch end;
                  ';'  : begin emit1 (semicolon1); nextch end;
                  '|'  : begin emit1 (bar1); nextch end;
                  ':'  : begin
                           nextch;
                           if ch<>'='
                           then emit1 (colon1)
                           else begin emit1 (becomes1); nextch end;
                         end;
                  '>'  : begin
                           nextch;
                           if ch<>'='
                           then emit1 (greater1)
                           else begin emit1 (notless1); nextch end;
                         end;
                  '<'  : begin
                           nextch;
                           case ch of
                             '=' :  begin emit1 (notgreater1); nextch end;
                             '>' :  begin emit1 (notequal1); nextch end;
                             else emit1 (less1)
                           end
                         end;
                  '.'  : if afterperiod
                         then begin
                                emit1 (doubledot1);
                                nextch;
                                afterperiod := false
                              end
                         else begin
                                nextch;
                                if ch <>'.'
                                then emit1 (period1)
                                else begin
                                       emit1 (doubledot1);
                                       nextch
                                     end
                              end;
                  else  if ch <> eot then begin emit1 (unknown1); nextch end
               end
    end;

  { Program = TokenField [ TokenField ]* }
  begin
    try
      currpass := scanp;

      init;

      emit2 (newline1, currl);
      while ch <> eot do nexttoken;
      emit1 (endtext1);

      done;

    except on e:exception do begin done; raise exception.create ('') end
    end
  end;

begin
end.