This is adaptation of super pascal compiler/interpreter, written by Per Brinch Hansen for sun workstations. I have modified it so that it can be run on freepascal on various contemporary platforms.

There are some cosmetic and some functional changes.
Most important functional is that compiler stops on first error.

In order to run super pascal on your platform of choice, you need to compile compiler.pas and interpret.pas by freepascal compiler. Afterwards you can compile program with .p suffix (e.g. fourier.p) by `bin/compile prog_name_without_.p`, and run it through interpreter by `bin/interpret prog_name_without_extension`, for users on bash shell, there is run.sh which can be used to compile, run and clean-up afterwards in single step.

Compile takes one parameter - file name and produce binary in .b file.

Interpret takes one to three parameters - file name (compulsory), input file name and output file name (optional) and run it. `keyboard` and `screen` can be used as file names for standard input and output.
