#!/bin/bash
bin/compile $1 $2 && bin/interpret $1 && rm $1.b
if [ -z "$2" ]
  then
    rm $1.log  2>/dev/null
fi
